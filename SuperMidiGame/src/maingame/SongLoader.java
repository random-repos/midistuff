package maingame;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import com.nosuch.midi.MidiMsg;
import com.nosuch.midi.MidiNote;
import com.nosuch.midi.MidiPhrase;
import com.nosuch.midi.MidiPhraseEnumeration;

//this class loads a song and:
//creates a vector of lists of notes
//the vector starts at lowest pitch, ends at highest
//the vector will have empty lists

public class SongLoader {
	static public class PhraseInfo
	{
		//number of channels including empty ones
		public int realChannels;
		//what channel (pitch) we start at
		public int start;
	}
	
	static PhraseInfo getPhraseInfo(MidiPhrase aphrase)
	{
		if(aphrase.size() < 1)
			return null;
		
		Set<Integer> intSet = new HashSet<Integer>();
		for(int i = 0; i < aphrase.size(); i++)
		{
			//if it's an note, add its pitch
			if(aphrase.getAt(i) instanceof MidiNote)
				intSet.add(aphrase.getAt(i).getPitch());
		}
		
		int max = 0;
		int min = 9999;
		for(int c : intSet)
		{
			if(c < min)
				min = c;
			if(c > max)
				max = c;
		}
		
		SongLoader.PhraseInfo tempInfo = new SongLoader.PhraseInfo();
		//this should probably fix itself to octaves
		tempInfo.realChannels = max-min + 1;
		tempInfo.start = min;
		
		return tempInfo;
	}
	
	static int countChannels(MidiPhrase aphrase)
	{
		Set<Integer> intSet = new HashSet<Integer>();
		for(int i = 0; i < aphrase.size(); i++)
			intSet.add(aphrase.getAt(i).getPitch());
		return intSet.size();
	}
	
	static int getChannelHigh(MidiPhrase aphrase)
	{
		Set<Integer> intSet = new HashSet<Integer>();
		for(int i = 0; i < aphrase.size(); i++)
			intSet.add(aphrase.getAt(i).getPitch());
		return intSet.size();
	}
	
	
	//probably don't need this function
	static Vector<List<MidiMsg>> loadSong(String filename)
	{
		//figure out which pitches are needed
		
		//construct vector of list with each pitch
		
		return null;
	}
}
