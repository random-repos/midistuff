package maingame;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sound.midi.*;
import javax.sound.midi.MidiSystem;

import processing.core.*;
import com.nosuch.midi.*;

public class MainClass extends PApplet
{
	static float speedRatio = 3;
	
	//public static void main(String args[])	{ PApplet.main(new String[] { "--present", "jeng_game.MainGame" } );	}
	MidiPhrase myPhrase;
	MidiPhraseEnumeration myEnum;
	MidiDevice myDevice;
	Receiver myReceiver;
	MidiMsg myMsg;
	
	SongLoader.PhraseInfo songInfo;
	
	public void setup()
	{
		 //set up a screen
		 //size(screen.width, screen.height, OPENGL);		
		 size(1440,600);
		 frameRate(60);
		 
		 MidiDevice.Info infos[] = MidiSystem.getMidiDeviceInfo();
		 for(MidiDevice.Info data : infos)
			 println(data.getName());
		 
		 try {
	        myDevice = MidiSystem.getMidiDevice(infos[1]);
		 } catch (MidiUnavailableException e) {
	    	e.printStackTrace();
		 }
		 
		 if (!(myDevice.isOpen())) {
		    try {
		      myDevice.open();
		      myReceiver = myDevice.getReceiver();
		    } catch (MidiUnavailableException e) {
			  e.printStackTrace();
		    }
		 }
		 
		 //LOAD THE FILE
		 try{
			 myPhrase = MidiFileReader.readMidiPhrase("abc.mid");
			 myEnum = (MidiPhraseEnumeration) myPhrase.getEnumeration();
			 
			 //get info
			 songInfo = SongLoader.getPhraseInfo(myPhrase);
			 
			 
		
		 } catch (MidiException e){
			 e.printStackTrace();
		 } catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		 
		 	
		 
		 
		 

	}
	
	public void finalize()
	{
	}
	
	public void draw()
	{
		background(127);
		for(int i = 0; i < songInfo.realChannels; i++)
		{
			stroke(127);
			rect(i*20 + 10, 500, 15, 5);
		}
		
		int j = 0;
		while(j < myPhrase.size() && myPhrase.getAt(j).getTime() < millis()/speedRatio + 2000/speedRatio)
		{
			if(myPhrase.getAt(j) instanceof MidiNoteOn )
			{
				println("hi");
			}
			else
				rect((myPhrase.getAt(j).getPitch()-songInfo.start)*20 + 10, 500 - (myPhrase.getAt(j).getTime() - millis()/speedRatio), 15, 5);
			j++;
		}
		
		/*
		while(myPhrase.getAt(0).getTime() < millis()speedRatio - 200speedRatio)
		{
			myPhrase.removeFirstMsg();
		}*/
	
		if(myMsg == null)
		{
			do
			{
				myMsg = (MidiMsg) myEnum.nextElement();
			}while(myMsg.getTime()==0);
		}
	
		while(myEnum.hasMoreElements() && myMsg.getTime() < millis()/speedRatio)
		{
			playAndPrint(myMsg, (int)(100-(myMsg.getTime()-millis()/speedRatio))*1000);
			myMsg = (MidiMsg) myEnum.nextElement();
		}
		
		
		//for(LinkedList<MidiMsg> l : noteMap)\
	}
	//put to handle input function in MainFunctions
	public void keyPressed()
	{
		/*
		if(myEnum.hasMoreElements())
		{
				MidiMsg theMsg = (MidiMsg)myEnum.nextElement();
				playAndPrint(theMsg);
		}
		*/
	}
	
	public void keyReleased()
	{
	}
	
	public void mousePressed()
	{
	}
	
	public void playAndPrint(MidiMsg theMsg)
	{
		playAndPrint(theMsg, (long)0);
	}
	public void playAndPrint(MidiMsg theMsg, long offset)
	{
		myReceiver.send(new EasyMessage(theMsg.getBytesOn()), (long)0);
		//println(theMsg.toString());
	}
}


