package mainprogram;
import processing.core.PApplet;
import themidibus.*;

public class InputListener implements SimpleMidiListener {
	
	MainClass p;
	public InputListener(MainClass _p)
	{
		p = _p;
	}
	
	public void controllerChange(int channel, int number, int value)
	{
		// Receive a controllerChange
		PApplet.println();
		PApplet.println("Controller Change:");
		PApplet.println("--------");
		PApplet.println("Channel:"+channel);
		PApplet.println("Number:"+number);
		PApplet.println("Value:"+value);
	}
	
	public void noteOn(int channel, int pitch, int velocity)
	{
		// Receive a noteOn
		PApplet.println();
		PApplet.println("Note On:");
		PApplet.println("--------");
		PApplet.println("Channel:"+channel);
		PApplet.println("Pitch:"+pitch);
		PApplet.println("Velocity:"+velocity);	
	}
	
	public void noteOff(int channel, int pitch, int velocity)
	{
		// Receive a noteOff
		PApplet.println();
		PApplet.println("Note Off:");
		PApplet.println("--------");
		PApplet.println("Channel:"+channel);
		PApplet.println("Pitch:"+pitch);
		PApplet.println("Velocity:"+velocity);
	}

}
