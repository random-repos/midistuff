package mainprogram;

import processing.core.*;
import themidibus.*;

public class MainClass extends PApplet
{
	//public static void main(String args[])	{ PApplet.main(new String[] { "--present", "jeng_game.MainGame" } );	}
	
	MidiBus myBus;
	SimpleMidiListener myListener;
	public void setup()
	{
		 //set up a screen
		 //size(screen.width, screen.height, OPENGL);		
		 size(600,480);
		 
		 MidiBus.list();
		 
		 myBus = new MidiBus(this, "", "JavaSoundSynthesizer");
		 myListener = new InputListener(this);
		 myBus.addMidiListener(myListener);
	}
	
	public void draw()
	{
	}
	//put to handle input function in MainFunctions
	public void keyPressed()
	{
	}
	
	public void keyReleased()
	{
	}
	
	public void mousePressed()
	{
	}
}


