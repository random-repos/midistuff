package maingame;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Vector;

import javax.sound.midi.*;

import com.nosuch.midi.*;

public class EasyFileReader {
	static Sequence openFileMidiGetSequence(String filename)
	{
		Sequence s = null;
		try {
			MidiPhrase phrase = MidiFileReader.readMidiPhrase("03bumbee.mid");
			//merge all channels
			phrase.setChannel(1);
			MidiPhraseEnumeration tempEnum = (MidiPhraseEnumeration) phrase.getEnumeration();
			s = new Sequence(javax.sound.midi.Sequence.PPQ, 96,1);
			
			while(tempEnum.hasMoreElements())
			{
				MidiMsg nsmsg = ((MidiMsg)tempEnum.nextElement()); 
				
				//create a new midimessage and add event to track
				MidiMessage jmsg = new EasyMessage(nsmsg.getBytesOn());
				s.getTracks()[0].add(new MidiEvent(jmsg, 0));
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		return s;
	}
	
	static List<Integer> countChannels(MidiPhrase aphrase)
	{
		List<Integer> numList = new Vector<Integer>();
		MidiPhraseEnumeration tempEnum = (MidiPhraseEnumeration) aphrase.getEnumeration();
		while(tempEnum.hasMoreElements())
		{
			int chn = ((MidiMsg)tempEnum.nextElement()).getChannel();
			boolean has = false;
			for(int num : numList)
				if(num == chn)
					has = true;
					
			if(!has)
				numList.add(chn);
		}
		return numList;
		
	}
}
