package maingame;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sound.midi.*;
import javax.sound.midi.MidiSystem;

import processing.core.*;
import com.nosuch.midi.*;

public class MainClass extends PApplet
{
	//public static void main(String args[])	{ PApplet.main(new String[] { "--present", "jeng_game.MainGame" } );	}
	MidiPhrase myPhrase;
	MidiPhraseEnumeration myEnum;
	Win32MidiOutput myOut;
	MidiDevice myDevice;
	Receiver myReceiver;
	MidiMsg myMsg;
	
	Map<Integer, List<MidiMsg>> noteMap = new HashMap<Integer, List<MidiMsg>>();
	List<Integer> pitchList = new LinkedList<Integer>();
	int numberOfKeys;
	
	public void setup()
	{
		 //set up a screen
		 //size(screen.width, screen.height, OPENGL);		
		 size(600,480);
		 
		 MidiDevice.Info infos[] = MidiSystem.getMidiDeviceInfo();
		 for(MidiDevice.Info data : infos)
			 println(data.getName());
		 
		 try {
	        myDevice = MidiSystem.getMidiDevice(infos[3]);
		 } catch (MidiUnavailableException e) {
	    	e.printStackTrace();
		 }
		 
		 if (!(myDevice.isOpen())) {
		    try {
		      myDevice.open();
		      myReceiver = myDevice.getReceiver();
		    } catch (MidiUnavailableException e) {
			  e.printStackTrace();
		    }
		 }
		 
		 //LOAD THE FILE
		 try{
			 myPhrase = MidiFileReader.readMidiPhrase("03bumbee.mid");
			 myEnum = (MidiPhraseEnumeration) myPhrase.getEnumeration();
			 
			 //set up a map
			 MidiPhraseEnumeration myEnum2 = (MidiPhraseEnumeration) myPhrase.getEnumeration();
			 while(myEnum2.hasMoreElements())
			 {
				MidiMsg myMsg2 = (MidiMsg) myEnum2.nextElement();
				if(!noteMap.containsKey(myMsg2.getPitch()))
					noteMap.put(myMsg2.getPitch(), new LinkedList<MidiMsg>());
				noteMap.get(myMsg2.getPitch()).add(myMsg2);
			 }
			 
		
		 } catch (MidiException e){
			 e.printStackTrace();
		 } catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		 
		 	
		 
		 
		 

	}
	
	public void finalize()
	{
	}
	
	public void draw()
	{
		
		if(myMsg == null)
		{
			do
			{
				myMsg = (MidiMsg) myEnum.nextElement();
			}while(myMsg.getTime()==0);
		}
		
		while(myEnum.hasMoreElements() && myMsg.getTime() < millis()/5)
		{
			playAndPrint(myMsg, (100-(myMsg.getTime()-millis()/5))*1000);
			myMsg = (MidiMsg) myEnum.nextElement();
		}
		
		
		//for(LinkedList<MidiMsg> l : noteMap)\
	}
	//put to handle input function in MainFunctions
	public void keyPressed()
	{
		/*
		if(myEnum.hasMoreElements())
		{
				MidiMsg theMsg = (MidiMsg)myEnum.nextElement();
				playAndPrint(theMsg);
		}
		*/
	}
	
	public void keyReleased()
	{
	}
	
	public void mousePressed()
	{
	}
	
	public void playAndPrint(MidiMsg theMsg)
	{
		playAndPrint(theMsg, (long)0);
	}
	public void playAndPrint(MidiMsg theMsg, long offset)
	{
		myReceiver.send(new EasyMessage(theMsg.getBytesOn()), (long)0);
		println(theMsg.toString());
	}
}


